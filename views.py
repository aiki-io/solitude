from flask import render_template, request, flash, url_for, redirect
from flask.ext.login import login_user, logout_user

from app import app
from forms import LoginForm, SignupForm
from models import User



@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login/', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        form = LoginForm()
        return render_template('login.html', form = form)
    else:
        form = LoginForm(request.form)
        if form.validate():
            login_user(form.user, remember= form.remember_me.data)
            flash('Successfully logged in as {}'.format(form.user.email), 'success')
            return redirect(request.args.get('next') or url_for('index'))
        else:
            return render_template('login.html', form = form)

@app.route('/logout')
def logout():
    logout_user()
    flash('You have been logged out.', 'success')
    return redirect(request.args.get('next') or url_for('index'))

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'GET':
        form = SignupForm()
        return render_template('signup.html', form = form)
    else:
        form = SignupForm(request.form)
        if form.validate():
           login_user(form.user)
           flash('Successfully signed up as {}'.format(form.user.email), 'success')
           return render_template('index.html')
        else:
            return render_template('signup.html', form = form)


@app.route('/profile', methods=['GET'])
def profile():
    return render_template('profile.html')





import datetime
from app import db, bcrypt, login_manager

class Game(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    name = db.Column(db.String(140))
    url = db.Column(db.String(140), default=None)



    def __repr__(self):
        return self.name

    def __iter__(self):
        yield ('id', self.id)
        yield('name', self.name)
        yield('url', self.url)
        yield('created_at', self.created_at.isoformat())
        yield('updated_at', self.updated_at.isoformat())


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)
    email = db.Column(db.String(64), unique=True)
    password_hash = db.Column(db.Binary(60))
    admin = db.Column(db.Boolean, default= False)

    def __repr__(self):
        return self.email


    @staticmethod
    def make_password(plaintext):
        return bcrypt.generate_password_hash(plaintext)

    def check_password(self, raw_password):
        return bcrypt.check_password_hash(self.password_hash, raw_password)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def is_admin(self):
        return self.admin

    def get_id(self):
        return str(self.id)


    @classmethod
    def create(cls, email, password, **kwargs):
        return User(email=email, password_hash = User.make_password(password), **kwargs)

    @staticmethod
    def authenticate(email, password):
        user = User.query.filter(User.email == email).first()
        if user and user.check_password(password):
            return user
        return False

    @login_manager.user_loader
    def user_loader(user_id):
        return User.query.get(user_id)
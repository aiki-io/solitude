import os

class Config:
    APP_DIR = os.path.dirname(os.path.realpath(__name__))
    STATIC_DIR=os.path.join(APP_DIR, 'static')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class DevConfig(Config):
    DEBUG = True
    MINIFY_PAGE = False
    SECRET_KEY = b'iv\x0cV^\x05\xca\xde\x82\xaf\xf0\x8d\xfe-\x7f\xa6G\x1f\n\xe0\x8cF"\xf0'
    SQLALCHEMY_DATABASE_URI='postgresql://bender:Ki;;A;;Human5@localhost/solitude'


class ProdConfig(Config):
    DEBUG = False
    MINIFY_PAGE = True
    SECRET_KEY = b'iv\x0cV^\x05\xca\xde\x82\xaf\xf0\x8d\xfe-\x7f\xa6G\x1f\n\xe0\x8cF"\xf0'
    SQLALCHEMY_DATABASE_URI='postgresql://bender:Ki;;A;;Human5@localhost/solitude'
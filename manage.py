#!/usr/bin/env python3
import getpass
import sys
from flask.ext.script import Manager, Command
from flask.ext.migrate import MigrateCommand
from app import app, db
from admin import admin
import views
import models
from models import User

manager = Manager(app)

manager.add_command('db', MigrateCommand)

@manager.shell
def make_shell_context():
    return dict(app =app, db = db)


class SuperuserCommand(Command):
    "creates a superuser"
    def run(self):
        email =input("email: ")
        pwd1 = getpass.getpass("Password: ")
        pwd2 = getpass.getpass("Repeat Password: ")
        if pwd1 != pwd2:
            print("Passwords don't match")
            sys.exit(1)
        user = User.create(email, pwd1)
        user.admin = True
        db.session.add(user)
        db.session.commit()
        if User.authenticate(email, pwd1) and user.is_admin():
            print("success")
        else:
            print ("failed to create superuser")



manager.add_command('superuser', SuperuserCommand())

if __name__ == '__main__':
    manager.run()

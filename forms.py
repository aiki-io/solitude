import wtforms
from wtforms import validators
from models import User
from app import db

class LoginForm(wtforms.Form):
    email = wtforms.StringField('Email', validators=[validators.DataRequired()])
    password = wtforms.PasswordField('Password', validators=[validators.DataRequired()])
    remember_me = wtforms.BooleanField('Remember me?', default=True)

    def validate(self):
        if not super(LoginForm, self).validate():
            return False
        self.user = User.authenticate(self.email.data, self.password.data)
        if not self.user:
            self.email.errors.append('Invalid email or password')
            return False
        return True


class SignupForm(wtforms.Form):
    email = wtforms.StringField('Email', validators= [validators.DataRequired(), validators.Email()])
    password = wtforms.PasswordField('Password', validators = [validators.DataRequired()])
    password2 = wtforms.PasswordField('Repeat Password', validators= [validators.DataRequired()])

    def validate(self):
        if not super(SignupForm, self).validate():
            return False
        if self.password.data != self.password2.data:
            self.password2.errors.append("Passwords don't match")
            return False
        user = User.create(self.email.data, self.password.data)
        db.session.add(user)
        db.session.commit()
        self.user = User.authenticate(self.email.data, self.password.data)
        if not self.user:
            return False
        return True




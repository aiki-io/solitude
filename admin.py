from flask import g, redirect, url_for, request
from flask_admin import Admin, expose, AdminIndexView
from flask.ext.admin.contrib.sqla import  ModelView
from wtforms import PasswordField
from app import app, db
from models import Game, User



class AdminAuthentication:
    def is_accessible(self):
        return g.user.is_authenticated and g.user.is_admin()

class IndexView(AdminIndexView):
    @expose('/')
    def index(self):
        if not (g.user.is_authenticated and g.user.is_admin()):
            return redirect(url_for('login', next = request.path))
        return self.render('admin/index.html')


admin = Admin(app, name='solitude', template_mode='bootstrap3', index_view=IndexView())

class BaseModelView(AdminAuthentication, ModelView):
    pass

class GameView(BaseModelView):
    column_list = ['id', 'name',  'url', 'created_at', 'updated_at']
    column_searchable_list = ['name', 'created_at', 'updated_at']
    form_columns =  ['name', 'url', 'created_at', 'updated_at']
    form_widget_args = {
        'id': {
            'readonly': True
        },
        'created_at': {
            'disabled': True
        },
        'updated_at': {
            'disabled': True
        }
    }



class UserView(BaseModelView):
    column_list = ['id', 'email', 'admin', 'created_at', 'updated_at']
    column_searchable_list = ['email']
    form_columns = [ 'email', 'password', 'admin', 'created_at', 'updated_at']
    form_extra_fields = {
        'password': PasswordField('New password')
    }

    form_widget_args = {
            'created_at': {
                'disabled': True
            },
            'updated_at': {
                'disabled': True
            },
    }

    def on_model_change(self, form, model, is_created):
        model.password_hash = User.make_password(form.password.data)
        return super(UserView, self).on_model_change(form, model, is_created)



admin.add_view(GameView(Game, db.session))
admin.add_view(UserView(User, db.session))
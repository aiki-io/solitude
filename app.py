import os
import sys
import logging
from logging import StreamHandler
from logging.handlers import RotatingFileHandler

from flask import Flask, g
from flask.ext.login import LoginManager, current_user
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.bcrypt import Bcrypt
from flask.ext.htmlmin import HTMLMIN
from flask.ext.assets import Environment, Bundle
from flask.ext.migrate import Migrate
from flask.ext.restful import Api
from config import DevConfig, ProdConfig

app = Flask(__name__, static_url_path='')
app.logger.addHandler(RotatingFileHandler('solitude.log'))
config = os.environ.get('SOLITUDE_CONFIG', 'DEV')
app.logger.setLevel(logging.INFO)
app.logger.info('applying config: "{}"'.format(config))

if config=='DEV':
    app.config.from_object(DevConfig)
elif config == 'PROD':
    app.config.from_object(ProdConfig)
else:
    app.logger.error('unknown config: "{}"'. format(config))
    sys.exit(0)

HTMLMIN(app)

db = SQLAlchemy(app)
migrate = Migrate(app, db)
api = Api(app)


bcrypt = Bcrypt(app)
login_manager= LoginManager(app)
login_manager.login_view = 'login'

@app.before_request
def before_request():
    g.user = current_user

env = Environment(app)
env.load_path = [
    os.path.join(os.path.dirname(__file__), 'bower_components'),
]

env.register('js_all', Bundle('jquery/dist/jquery.min.js', 'bootstrap/dist/js/bootstrap.min.js'), output='js_all.js')
env.register('css_all', Bundle('bootstrap/dist/css/bootstrap.min.css'), output='css_all.css')

import api
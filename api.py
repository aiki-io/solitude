from flask import jsonify
from flask.ext.restful import Resource
from app import api
from models import Game

class GamesResource(Resource):
    def get(self):
        return { 'games': dict(game) for game in Game.query.all() }

class GameResource(Resource):
    def get(self, id):
        game = Game.query.get_or_404(id)
        return dict(game)


api.add_resource(GamesResource, '/api/games', endpoint ='games')
api.add_resource(GameResource, '/api/games/<int:id>', endpoint = 'game')
